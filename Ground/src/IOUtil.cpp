#include "IOUtil.h"

#include <iostream>
#include <fstream>

using namespace std;

vector<string> ReadFile(const string &path)
{
    vector<string> result;
    string s;

    ifstream in(path);
    if (in)
        while (getline(in, s))
            result.push_back(s);

    return result;
}

void WriteFile(const string &path, vector<string> &write)
{
    ofstream output(path);
    if (output)
        for (auto s : write)
            output << s << endl;
}