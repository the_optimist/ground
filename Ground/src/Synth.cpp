#include "Synth.h"

#include "Constants.h"
#include "Voice.h"
#include "Note.h"
#include "UtilityMath.h"

#include <chrono>

Synth::Synth()
{

}

Synth::Synth(Timbre t, ADSRfilter _adsr)
    : timbre(t), adsr(_adsr)
{
}

const float Synth::ProduceSound()
{
    float result = 0.0f;

    auto it = voices.begin();
    auto end = voices.end();

    while (it != end)
    {
        Voice &v = *it;

        if (!v.isHeld)
        {
            v.velocity = Lerp(v.releaseVelocity, 0.0f, v.timeHeld / adsr.releaseTime);
        }
        else
        {
            if (v.timeHeld <= adsr.attackTime)
            {
                v.velocity = Lerp(v.releaseVelocity, v.strikeVelocity, v.timeHeld / adsr.attackTime);
            }
            else if (v.timeHeld <= adsr.attackTime + adsr.decayTime)
            {
                v.velocity = Lerp( v.strikeVelocity, v.strikeVelocity * adsr.sustainLevel, (v.timeHeld - adsr.attackTime) / adsr.decayTime);
            }
            else
            {
                //v.freqMod = 1.0f + 0.05f * sinf(v.timeHeld * 20);
            }
        }

        if (!v.isHeld && v.velocity < 0.001f)
        {
            it = voices.erase(it);
        }
        else
        {
            result += v.velocity * v.GetImpulse(&timbre);
            v.timeHeld += TIME_STEP;
            ++it;
        }
    }

    return result;
}

void Synth::StrikeNote(const Note &note, const float velocity)
{
    for (auto it = voices.begin(); it != voices.end(); ++it)
    {
        if (it->note == note)
        {
            it->isHeld = true;
            it->timeHeld = 0.0f;
            it->releaseVelocity = it->velocity;
            it->strikeVelocity = velocity;
            return;
        }
    }
    voices.push_back(Voice(note, velocity));
}

void Synth::ReleaseNote(const Note &note)
{
    for (auto it = voices.begin(); it != voices.end(); ++it)
    {
        if (it->note == note)
        {
            it->isHeld = false;
            it->timeHeld = 0.0f;
            it->releaseVelocity = it->velocity;
        }
    }
}