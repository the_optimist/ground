#include "Voice.h"

#include "Constants.h"
#include "Timbre.h"
#include "Note.h"

Voice::Voice(Note _note, float _velocity)
    : note(_note), velocity(0.0f), freqMod(1.0f), isHeld(true), timeHeld(0.0f),
    releaseVelocity(0.0f), strikeVelocity(_velocity), phase(0.0f)
{

}

const float Voice::GetImpulse(Timbre *pTimbre)
{
    phase += TIME_STEP * note.frequency * freqMod;
    // Avoid fp imprecision by normalizing to [0,1).
    phase -= (float)(int)(phase);

    return velocity * pTimbre->GetImpulse(phase);
}