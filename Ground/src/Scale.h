#ifndef GROUND_SCALE_H
#define GROUND_SCALE_H

#include <vector>

#include "Note.h"
#include "Chord.h"

enum ScaleIntervals
{
    SI_UNISON = 0,
    SI_SECOND,
    SI_THIRD,
    SI_FOURTH,
    SI_FIFTH,
    SI_SIXTH,
    SI_SEVENTH,
    SI_OCTAVE,
    SI_NINE,
    SI_ELEVEN = 10,
    SI_THIRTEEN = 12
};

struct Scale
{
    Notes rootNote;
    std::vector<int> noteOffsets;

    Scale()
    {}

    Scale(Notes root, std::vector<int> offsets)
        : rootNote(root), noteOffsets(offsets)
    {}

    int GetRootDistance(Notes note)
    {
        int i = note - rootNote;
        while (i < 0)
            i += 12;

        return i;
    }

    NoteIntervals GetNoteInterval(ScaleIntervals start, ScaleIntervals move)
    {
        int note = (noteOffsets[(start + move) % noteOffsets.size()] - noteOffsets[start]);
        if (note < 0)
        {
            note += 12;
        }
        else if (note >= 12)
        {
            note -= 12;
        }
        return NoteIntervals(note);
    }

    Notes GetNote(ScaleIntervals interval)
    {
        return Notes((rootNote + noteOffsets[interval]) % 12);
    }

    Notes GetNote(int intervalIndex)
    {
        return Notes((rootNote + noteOffsets[intervalIndex]) % 12);
    }

    /* Gets the index of the note in the scale.
       If the note lies in between two scale notes, the lower is chosen.
    */
    int GetIndexDown(Notes note)
    {
        int distanceToRoot = GetRootDistance(note);

        int length = noteOffsets.size();

        // Adjust to closest (downwards)
        for (int i = 0; i != length; ++i)
        {
            int offset = noteOffsets[i];
            if (distanceToRoot == offset)
            {
                return i;
            }
            else if (distanceToRoot < offset)
            {
                return i - 1;
            }
        }
        return length - 1;
    }

    /* Gets the index of the note in the scale.
       If the note lies in between two scale notes, the upper is chosen.
    */
    int GetIndexUp(Notes note)
    {
        int distanceToRoot = GetRootDistance(note);

        int length = noteOffsets.size();

        for (int i = 0; i != length; ++i)
        {
            int offset = noteOffsets[i];
            if (distanceToRoot <= offset)
            {
                return i;
            }
        }
        return length - 1;
    }

    Note Ascend(Note note, ScaleIntervals interval)
    {
        int length = noteOffsets.size();

        int pos = GetIndexDown(note.index);

        // Ascend in scale
        int ascended = 0;
        for (int i = 0; i != interval; ++i)
        {
            if (pos + 1 >= length)
            {
                ascended += 12 - noteOffsets[pos];
                pos = 0;
            }
            else
            {
                ascended += noteOffsets[pos + 1] - noteOffsets[pos];
                ++pos;
            }
        }

        note += ascended;
        return note;
    }

    Note Descend(Note note, ScaleIntervals interval)
    {
        int length = noteOffsets.size();

        int pos = GetIndexUp(note.index);

        // Descend in scale
        int descended = 0;
        for (int i = 0; i != interval; ++i)
        {
            if (pos - 1 < 0)
            {
                pos = length - 1;
                descended += 12 - noteOffsets[pos];
            }
            else
            {
                descended += noteOffsets[pos] - noteOffsets[pos - 1];
                --pos;
            }
        }

        note -= descended;
        return note;
    }

	/*static const std::string ModeIndexToRomanString(int modeIndex)
	{
		std::string numerals[] = { "I", "ii", "iii", "IV", "V", "vi", "vii" };

		return numerals[modeIndex];
	}

	static const std::string ModeIndexToNameString(int modeIndex)
	{
		std::string names[] = { "Ionian", "Dorian", "Phrygian", "Lydian", "Mixolydian", "Aeolian", "Locrian" };

		return names[modeIndex];
	}*/
};

#endif // !GROUND_SCALE_H