#ifndef GROUND_MIDI_H
#define GROUND_MIDI_H

#include <string>
#include <iostream>
#include <fstream>
#include <exception>

struct MIDIHeader
{
    // spell MThd
    char M;
    char T;
    char h;
    char d;

    unsigned int headerLength; // always 6
    
    // 0 = single track
    // 1 = multi track
    // 2 = multi song (i.e. a series of type 0 tracks)
    short format;

    short n; // the number of track chunks after header
    
    // delta time:
    // > 0 => measures ticks/beat,
    // < 0 => SMPTE compatible units
    short division;
};

struct MIDITrackEvent
{

};

struct MIDITrackChunk
{
    // spell MTrk
    char M;
    char T;
    char r;
    char k;
    // number of bytes of data following
    int length;
};

MIDIHeader ReadMIDIHeader(std::istream &r);

void TestMIDIRead();


#endif // !GROUND_MIDI_H
