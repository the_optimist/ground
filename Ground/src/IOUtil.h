#ifndef GROUND_IO_UTIL_H
#define GROUND_IO_UTIL_H

#include <string>
#include <vector>

std::vector<std::string> ReadFile(const std::string &path);

void WriteFile(const std::string &path, std::vector<std::string> &write);

#endif // !GROUND_IO_UTIL_H
