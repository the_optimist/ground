#include "BasicStates.h"

#include <string>
#include <vector>
#include <thread>
#include <portaudio.h>

#include "Constants.h"
#include "Note.h"
#include "Scale.h"
#include "Voice.h"
#include "Synth.h"
#include "Random.h"
#include "Arranger.h"

#include "ConsoleHelper.h"
#include "CircularTripleAudioBuffer.h"
#include "Soloist.h"
#include "Composer.h"
#include "UtilityMath.h"

static CircularTripleAudioBuffer triBuffer;

struct MusicDataTemp
{
    // arrangement
    Arranger arranger;

    // composer
    Composer composer;

    // players
    std::vector<Soloist> soloists;
    std::vector<Synth> synths;
};

struct PortAudioData
{
    PaTime bufferStartTime;
    PaTime bufferDacTime;

    bool writing;
};

PortAudioData paData;
MusicDataTemp tempPlayData;

static int PortAudioCallback(const void *inputBuffer, void *outputBuffer,
    unsigned long framesPerBuffer,
    const PaStreamCallbackTimeInfo *timeInfo,
    PaStreamCallbackFlags statusFlags,
    void *userData)
{
    PortAudioData *data = (PortAudioData*)userData;
    float *out = (float*)outputBuffer;
    (void)inputBuffer; // Prevent unused variable warning.

    // Get timing info
    data->bufferStartTime = timeInfo->currentTime;
    data->bufferDacTime = timeInfo->outputBufferDacTime;

    // Copy data from current buffer
    data->writing = true;
    triBuffer.CopyFromBuffer(out, framesPerBuffer * 2 * sizeof(float));
    data->writing = false;

    return 0;
}

int PaErrorTest(PaError err)
{
    if (err != paNoError)
    {
        printf("PortAudio error: %s\n", Pa_GetErrorText(err));
        return 1;
    }
    return 0;
}

PlayState::PlayState()
{

}

PlayState::~PlayState()
{

}

void PlayState::Init()
{

}

const unsigned long FramesPerBuffer = 11025;
PaStream *stream;

PaTime prevTime = 0;

std::thread *playThread;
bool runThread;

const int skip = 10;
float skipped[skip * 2];
float convolution[2 * FramesPerBuffer];

void PlayMusic(bool *run)
{
    while (*run)
    {
        //PaTime time = Pa_GetStreamTime(stream);
        if (prevTime != paData.bufferStartTime && !paData.writing)
        {
            // buffer swap
            //printf("Swap! %d->%d\n", buffer, (buffer + 1) % 3);
            Pa_Sleep(1);
            prevTime = paData.bufferStartTime;
            triBuffer.SwapBuffer();
            float * out = triBuffer.GetNextBufferStart();

            // Fill buffer

            // Set volume and pan.
            auto &data = tempPlayData;
            float lVol = data.arranger.volume * (data.arranger.pan - 1.0f) * -0.5f;
            float rVol = data.arranger.volume * (data.arranger.pan + 1.0f) * 0.5f;

            for (int i = 0; i != FramesPerBuffer; i++)
            {
                data.composer.Compose(&data.arranger);

                for (int j = 0; j != data.soloists.size(); ++j)
                {
                    data.soloists[j].Improvise(&data.synths[j], &data.arranger, &data.composer);
                }

                // Synthesize
                float amplitude = 0;
                for (auto &synth : data.synths)
                {
                    amplitude += synth.ProduceSound();
                }
                amplitude /= data.synths.size();

                *out++ = lVol * amplitude;
                *out++ = rVol * amplitude;
            }

            out -= 2 * FramesPerBuffer;

            // temporary block convolution solution to filter out aliasing
            for (int i = 0; i != FramesPerBuffer; i++)
            {
                float *destination = convolution;
                destination[2 * i] = 0;
                destination[2 * i + 1] = 0;
                for (int j = 0; j < skip; ++j)
                {
                    int index = 2 * (i - j);
                    float *source = out;
                    if (index < 0)
                    {
                        source = skipped;
                        index += skip * 2;
                    }
                    destination[2 * i] += source[index];
                    destination[2 * i + 1] += source[index + 1];
                }
                destination[2 * i] /= skip;
                destination[2 * i + 1] /= skip;
            }
            
            memcpy(skipped, out + 2 * (FramesPerBuffer - skip), 2 * skip * sizeof(float));
            memcpy(out, convolution, 2 * FramesPerBuffer * sizeof(float));
        }
    }
}

void PlayState::OnEnter()
{
    Console::Tell("---PLAY STATE---");
    menuState = false;

    Random rng;
    Console::TellF("Seed: %u", rng.Seed);

    int bpm = rng.NextInt(90, 150);
    Console::TellF("BPM: %i", bpm);

    //Console::Tell("0 : A\n1 : Bb\n2 : B\n3 : C\n4 : Db\n5 : D");
    //Console::Tell("6 : Eb\n7 : E\n8 : F\n9 : Gb\n10: G\n11: Ab");
    //int userTonic = Console::IntAsk("Choose tonic: ", false);
    int tonic = rng.NextInt(12);
    Console::TellF("Tonic: %s", Note::GetNoteName((Notes)tonic, (Notes)tonic).c_str());

	/*std::string chordSeqStr = Console::StrAsk("Specify chord sequence: ", false);
	std::vector<std::string> chordSeqStrs = StrSplit(chordSeqStr, ' ');
	std::vector<int> chordSeq;
	for (std::string s : chordSeqStrs)
	{
		int chordIndex = 0;
		if (StrToInt(s, &chordIndex))
		{
			chordSeq.push_back(chordIndex);
		}
	}*/

    std::vector<int> chordSeq{ 0,3,6,2,5,1,4,0 };

    auto &data = tempPlayData;
    data.arranger = Arranger((float)bpm, 1.0f, 0.0f);

    data.soloists.push_back(Soloist((int)rng.Seed, 2, 5, ImprovisationType::IT_CHORDS));
    data.soloists.push_back(Soloist((int)rng.Seed + 1, 3, 6, ImprovisationType::IT_SOLO));
    data.soloists.push_back(Soloist((int)rng.Seed + 2, 1, 2, ImprovisationType::IT_SOLO));

    for (int j = 0; j != data.soloists.size(); ++j)
    {
        std::vector<float> amps;
        int harmonics = rng.NextInt(0,1024);
        for (int i = 0; i != harmonics; ++i)
            amps.push_back(rng.NextFloat() / (float)(pow(i + 1, 2)));
        data.synths.push_back(Synth(amps,
            ADSRfilter(rng.NextFloat() / 10, rng.NextFloat() / 10, rng.NextFloat(), rng.NextFloat())));
    }

	data.composer = Composer((int)rng.Seed, Notes(tonic), chordSeq);

    //for (int i = 0; i != 5; ++i)
    //    amps2.push_back(random.NextFloat());
    //data.synth = Synth(Timbre(amps2));
    
    triBuffer.Clear();

    if (PaErrorTest(Pa_Initialize()))
        return;

    // Open a stream
    PaErrorTest(Pa_OpenDefaultStream(&stream,
        0, // no input channels
        2, // stereo output
        paFloat32, // 32 bit floating point output
        SAMPLE_RATE,
        FramesPerBuffer,
        /* frames per buffer, i.e. the number of sample frames that
        PortAudio will request from the callback. Many apps may want
        to use paFramesPerBufferUnspecified, which tells PortAudio
        to pick the best, possibly changing, buffer size. */
        PortAudioCallback, // our callback
        &paData)); // pointer passed to callback

    // Start the stream!
    PaErrorTest(Pa_StartStream(stream));

    runThread = true;
    playThread = new std::thread(PlayMusic, &runThread);

    Console::Tell("OK! Enter \"exit\" to exit.");
}

void PlayState::OnExit()
{
    Console::Tell("");

    // stop threaded execution
    runThread = false;
    playThread->join();
    delete playThread;

    // Stop the stream
    PaErrorTest(Pa_StopStream(stream));

    // Close the stream too!
    PaErrorTest(Pa_CloseStream(stream));

    tempPlayData.soloists.clear();
    tempPlayData.synths.clear();

    // Terminate PortAudio.
    if (PaErrorTest(Pa_Terminate()))
        return;
}

void PlayState::Update()
{
    std::string text = Console::StrAsk("Cmd: ", true);

    if (text == "exit")
    {
        menuState = true;
    }
}