#include "StateMachine.h"
#include "BasicStates.h"

// BUGS:
/*
Glissando lerp sounds discrete-stepping - solved: floating point imprecision error
No sound in release mode - solved: with thread sleep 1 ms (why?)
""(enter)+"exit"(enter) doesn't exit play mode

*/

int main()
{
    StateMachine sm;
    
    MenuState ms;
    PlayState ps;
    TestState ts;

    {
        Transition t;
        // menu->play
        t.condition = &ms.playState;
        t.state = (State*)&ps;
        ms.AddTransition(t);

        // play->menu
        t.condition = &ps.menuState;
        t.state = (State*)&ms;
        ps.AddTransition(t);

        // menu->test
        /*t.condition = &ms.testState;
        t.state = (State*)&ts;
        ms.AddTransition(t);*/

        // test->menu
        t.condition = &ts.menuState;
        t.state = (State*)&ms;
        ts.AddTransition(t);
    }

    sm.SetState(&ms);

    while (!ms.quitState)
    {
        sm.Update();
    }

    return 0;
}