#ifndef GROUND_TIMBRE_H
#define GROUND_TIMBRE_H

#include<vector>

#define WAVETABLE_SIZE 1024

/*
The timbre struct is what makes one instrument sound different from another.
It shapes the sound, beyond what note is played.
*/

class Random;

struct Timbre
{
    float wavetable[WAVETABLE_SIZE];

    Timbre();
    Timbre(Random *rng);
    Timbre(std::vector<float> amps);

    const float GetImpulse(float phase) const;
};

#endif // !GROUND_TIMBRE_H