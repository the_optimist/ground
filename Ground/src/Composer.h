#ifndef GROUND_COMPOSER_H
#define GROUND_COMPOSER_H

#include <vector>

#include "Arranger.h"
#include "Random.h"
#include "Constants.h"
#include "Chord.h"
#include "Scale.h"
#include "ConsoleHelper.h"

#define BEATS_PER_BAR 4

/*
The composer writes the music before it is played, and uses patterns
to provide structure and a feeling of coherence to the music.
*/
class Composer
{
public:
    Chord chord;
    Scale scale;

    Composer()
        : barTimer(0.0f), barTime(0.0f), chord(),
        scale(Notes::C, { 0,2,4,5,7,9,11 }), chordSequence({ 0,5,3,4 }),
        chordIndex(0), rng(0)
    {
    }

	Composer(int seed, Notes tonic, std::initializer_list<int> chords)
		: barTimer(0.0f), barTime(0.0f), chord(),
		scale(tonic, { 0,2,4,5,7,9,11 }), chordSequence(chords),
		chordIndex(0), rng(seed)
	{
	}

	Composer(int seed, Notes tonic, std::vector<int> chords)
		: barTimer(0.0f), barTime(0.0f), chord(),
		scale(tonic, { 0,2,4,5,7,9,11 }), chordSequence(chords),
		chordIndex(0), rng(seed)
	{
	}

    ~Composer()
    {}

    void Compose(Arranger *arranger)
    {
        // Check note switch timer.
		if (barTimer >= barTime)
		{
			barTimer -= barTime;
			barTime = BEATS_PER_BAR * MINUTES_TO_SECONDS / (arranger->bpm);

			//int mode = rng.NextInt((int)scale.noteOffsets.size());
			int mode = chordSequence[chordIndex];
            printf("%i: ", chordIndex + 1);
			chordIndex = (chordIndex + 1) % chordSequence.size();
			chord.root = scale.GetNote(mode);

			chord.Clear();
			for (int i = 0; i < 3; ++i) // create a chord
			{
                chord.AddNoteInterval(scale.GetNoteInterval(ScaleIntervals(mode), ScaleIntervals(i * 2)));
			}
            if (rng.ChanceSuccess(0.5))
            {
                chord.AddNoteInterval(scale.GetNoteInterval(ScaleIntervals(mode), ScaleIntervals(rng.NextInt(ScaleIntervals::SI_NINE))));
            }

			printf("%s %s\t%s\n",
                Note::GetNoteName(chord.root, scale.rootNote).c_str(),
                chord.GetNotation().c_str(),
                chord.GetName().c_str());
        }

        barTimer += TIME_STEP;
    }

private:
    float barTimer;
    float barTime;

    std::vector<int> chordSequence;
    int chordIndex;

    Random rng;
};

#endif // !GROUND_COMPOSER_H