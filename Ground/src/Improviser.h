#ifndef GROUND_IMPROVISER_H
#define GROUND_IMPROVISER_H

#include "Constants.h"
#include "Scale.h"

/*
The improviser is a random walker who comes up with new ideas around a
concept. Composers, arrangers and soloists all use improvisation.
Effectively the abstraction of randomness.
*/
class Improviser
{
public:
    Improviser()
    {}
    virtual ~Improviser()
    {}

    virtual void Improvise(Synth *instrument, Arranger *arranger) = 0;

private:
    
};

#endif // !GROUND_IMPROVISER_H
