#ifndef GROUND_SYNTH_H
#define GROUND_SYNTH_H

#include <list>

#include "Timbre.h"
#include "Note.h"
#include "ADSRfilter.h"

struct Voice;
struct Note;

class Synth
{
public:
    Synth();
    Synth(Timbre t, ADSRfilter adsr);

    // Either add required note information from improviser,
    // or use start/stop note events while managing voices,
    // to make way for easy implementation of ADSR-filters et. al.
    const float ProduceSound();

    void StrikeNote(const Note &note, const float velocity);
    void ReleaseNote(const Note &note);

private:
    std::list<Voice> voices;
    Timbre timbre;
    ADSRfilter adsr;
};

#endif // !GROUND_SYNTH_H
