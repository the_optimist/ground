#include "Chord.h"

#include <algorithm>

bool Chord::Matches(FamousChord chord)
{
    if (chord.intervals.size() != intervals.size())
        return false;

    for (int i = 0; i != chord.intervals.size(); ++i)
    {
        bool matches = false;
        for (int j = 0; j != intervals.size(); ++j)
        {
            if (intervals[j] == chord.intervals[i])
                matches = true;
        }
        if (!matches)
            return false;
    }

    return true;
}

void Chord::AddNoteInterval(NoteIntervals interval)
{
    for (size_t i = 0; i < intervals.size(); ++i)
    {
        if (intervals[i] == interval)
            return;
    }

    intervals.push_back(interval);

    sort(intervals.begin(), intervals.end());
}

void Chord::Clear()
{
    intervals.clear();
}

Chord::Chord(std::vector<int> interv)
{
    std::sort(interv.begin(), interv.end()); // make sure the notes are in order

    for (int i : interv)
        intervals.push_back((NoteIntervals)i);
}

std::vector<Notes> Chord::GetNotes()
{
    std::vector<Notes> result;

    for (auto &i : intervals)
    {
        result.push_back(Notes((root + i) % 12));
    }

    return result;
}

std::string Chord::GetName()
{
    for (int i = 0; i < sizeof(famousChords) / sizeof(FamousChord); ++i)
    {
        FamousChord named = famousChords[i];
        if (Matches(named))
        {
            return named.name;
        }
    }

    return "";
}

std::string Chord::GetNotation()
{
    for (int i = 0; i < sizeof(famousChords) / sizeof(famousChords[0]); ++i)
    {
        FamousChord named = famousChords[i];
        if (Matches(named))
        {
            return named.notation;
        }
    }

    return "";
}