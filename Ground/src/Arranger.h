#ifndef GROUND_ARRANGER_H
#define GROUND_ARRANGER_H

class Arranger
{
public:
    float bpm;
    float volume;
    float pan;

    Arranger(float initialBpm, float initialVolume, float initialPan)
        : bpm(initialBpm), volume(initialVolume), pan(initialPan)
    {}

    Arranger()
        : bpm(120.0f), volume(0.3f), pan(0.0f)
    {}

    ~Arranger()
    {}

private:

};

#endif // !GROUND_ARRANGER_H
