#include "Soloist.h"

#include "Scale.h"
#include "Synth.h"
#include "Arranger.h"
#include "Composer.h"

Soloist::Soloist(int seed, int bottomOctave, int topmostOctave, ImprovisationType iT)
    : feel(1), feelCounter(0), noteTimer(0.0f), noteTime(0.0f), isQuiet(true),
    btmOctave(bottomOctave), topOctave(topmostOctave), rng(seed), type(iT)
{
    playedNotes.push_back( PlayedNoteInfo( Note( Notes::B, 3 ), 1.0f ) );
}

void Soloist::Improvise(Synth *synth, Arranger *arranger, Composer *composer)
{
    // Check note switch timer.
    if (noteTimer >= noteTime)
    {
        noteTimer -= noteTime;
        noteTime = MINUTES_TO_SECONDS / (arranger->bpm * feel);

        if (feelCounter >= feel)
        {
            // New measure
            if (type == ImprovisationType::IT_CHORDS)
            {
                feel = 1;
            }
            else if (type == ImprovisationType::IT_SOLO)
            {
                feel = rng.NextInt(1, 4);
            }
            feelCounter = 0;
        }

        if (type == ImprovisationType::IT_CHORDS)
        {
            for (auto &n : playedNotes)
            {
                synth->ReleaseNote(n.note);
            }

            playedNotes.clear();

            std::vector<Notes> notes = composer->chord.GetNotes();
            int order[] = { 0,2,1,3 };

            Note note = Note(notes[order[0]], btmOctave);
            float vel = 0.6f;
            playedNotes.push_back(PlayedNoteInfo(note, vel));
            Note previous = note;

            for (size_t i = 1; i < notes.size(); ++i)
            {
                int index = notes[order[i]];
                if (index < previous.index)
                    note = Note(Notes(index), previous.octave + 1);
                else
                    note = Note(Notes(index), previous.octave);
                playedNotes.push_back(PlayedNoteInfo(note, vel));
                previous = note;
            }
        }
        else if (type == ImprovisationType::IT_SOLO)
        {
            for (auto &n : playedNotes)
            {
                synth->ReleaseNote(n.note);
            }

            Note &note = playedNotes[0].note;

            if (feelCounter == 0 || rng.ChanceSuccess(0.5))
            {
                auto notes = composer->chord.GetNotes();
                int noteIndex = rng.NextInt(notes.size());

                if (rng.ChanceSuccess(0.5))
                {
                    note.index = notes[noteIndex];
                    note.Refresh();
                }
            }
            else
            {
                int travel = rng.NextInt(0, 2);
                bool ascend = rng.ChanceSuccess(0.5);

                if (ascend)
                    note = composer->scale.Ascend(note, (ScaleIntervals)travel);
                else
                    note = composer->scale.Descend(note, (ScaleIntervals)travel);
            }
        }

        for (auto &n : playedNotes)
        {
            if (type == ImprovisationType::IT_SOLO)
            {
                isQuiet = rng.ChanceSuccess(22);

                if (isQuiet)
                    n.velocity = 0.0f;
                else
                    n.velocity = 1.0f;//rng.NextFloat(0.5f, 0.7f);
            }

            if (n.note.octave < btmOctave)
            {
                n.note.octave = btmOctave;
                n.note.Refresh();
            }
            else if (n.note.octave > topOctave)
            {
                n.note.octave = topOctave;
                n.note.Refresh();
            }

            synth->StrikeNote(n.note, n.velocity);
        }

        ++feelCounter;
    }

    noteTimer += TIME_STEP;
}