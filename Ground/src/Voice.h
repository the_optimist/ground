#ifndef GROUND_VOICE_H
#define GROUND_VOICE_H

#include "Note.h"

struct Timbre;

struct Voice
{
public:
    Note note;
    float velocity;
    float freqMod;
    bool isHeld;
    float timeHeld;
    float releaseVelocity;
    float strikeVelocity;

    Voice(Note note, float velocity);

    const float GetImpulse(Timbre *pTimbre);

private:
    float phase;
};

#endif // !GROUND_VOICE_H