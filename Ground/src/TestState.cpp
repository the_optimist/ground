#include "BasicStates.h"

#include "ConsoleHelper.h"

#include "MIDI.h"

using namespace std;

TestState::TestState()
{

}

TestState::~TestState()
{

}

void TestState::Init()
{

}

void TestState::OnEnter()
{
    Console::Tell("---TEST STATE---");
    menuState = false;
}

void TestState::OnExit()
{
    Console::Tell("");
}

void TestState::Update()
{
    TestMIDIRead();
    menuState = true;
}