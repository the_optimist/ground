#ifndef GROUND_RANDOM_H
#define GROUND_RANDOM_H

#include <random>

class Random
{
public:
    typedef std::uniform_int_distribution<int> int_dist;
    typedef std::uniform_int_distribution<long> long_dist;
    typedef std::uniform_int_distribution<unsigned int> uint_dist;
    typedef std::uniform_int_distribution<unsigned long> ulong_dist;
    typedef std::uniform_real_distribution<float> float_dist;
    typedef std::uniform_real_distribution<double> double_dist;

    unsigned long long Seed;

    Random(const unsigned long long seed)
        : _rng(seed)
    {
        _int = int_dist();
        _long = long_dist();
        _uint = uint_dist();
        _ulong = ulong_dist();
        _float = float_dist();
        _double = double_dist();

        Seed = seed;
    }

    Random()
    {
        std::random_device rd;
        Seed = rd();
        _rng = std::mt19937_64(Seed);
        _int = int_dist();
        _long = long_dist();
        _uint = uint_dist();
        _ulong = ulong_dist();
        _float = float_dist();
        _double = double_dist();
    }

    inline const int NextInt() { return _int(_rng); }
    inline const long NextLong() { return _long(_rng); }
    inline const unsigned int NextUInt() { return _uint(_rng); }
    inline const unsigned long NextULong() { return _ulong(_rng); }
    inline const float NextFloat() { return _float(_rng); }
    inline const double NextDouble() { return _double(_rng); }

    inline const bool ChanceSuccess(const double percentage)
    {
        return NextDouble() < percentage;
    }

    inline const bool ChanceSuccess(const int percentage)
    {
        return NextDouble() < ((double)percentage / 100.0);
    }

    // Inclusive upper bound
    inline const int NextInt(const int min, const int max)
    {
        int_dist dist(min, max);
        return dist(_rng);
    }

    // Exclusive upper bound, for ease of use with array indexing
    inline const int NextInt(const int max)
    {
        int_dist dist(0, max - 1);
        return dist(_rng);
    }

    inline const long NextLong(const long min, const long max)
    {
        long_dist dist(min, max);
        return dist(_rng);
    }

    inline const unsigned int NextUInt(const unsigned int min, const unsigned int max)
    {
        uint_dist dist(min, max);
        return dist(_rng);
    }

    inline const unsigned long NextULong(const unsigned long min, const unsigned long max)
    {
        ulong_dist dist(min, max);
        return dist(_rng);
    }

    inline const float NextFloat(const float min, const float max)
    {
        float_dist dist(min, max);
        return dist(_rng);
    }

    inline const double NextDouble(const double min, const double max)
    {
        double_dist dist(min, max);
        return dist(_rng);
    }

private:
    std::mt19937_64 _rng;
    int_dist _int;
    long_dist _long;
    uint_dist _uint;
    ulong_dist _ulong;
    float_dist _float;
    double_dist _double;
};

#endif // !GROUND_RANDOM_H