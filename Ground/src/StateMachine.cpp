#include "StateMachine.h"

StateMachine::StateMachine()
{

}

StateMachine::~StateMachine()
{

}

void StateMachine::SetState(State *state)
{
    if (!state->initialized)
    {
        state->Init();
        state->initialized = true;
    }
    state->OnEnter();
    nowState = state;
}

void StateMachine::Update()
{
    nowState->Update();
    for (const Transition &t : nowState->transitions)
    {
        if (*(t.condition))
        {
            nowState->OnExit();
            SetState(t.state);
            break;
        }
    }
}