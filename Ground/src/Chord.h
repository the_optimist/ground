#ifndef GROUND_CHORD_H
#define GROUND_CHORD_H

#include <vector>
#include <algorithm>

#include "Note.h"

enum NoteIntervals
{
    UNISON = 0,
    MINOR_SECOND,
    MAJOR_SECOND,
    MINOR_THIRD,
    MAJOR_THIRD,
    PERFECT_FOURTH,
    TRITONE,
    PERFECT_FIFTH,
    MINOR_SIXTH,
    MAJOR_SIXTH,
    MINOR_SEVENTH,
    MAJOR_SEVENTH,
    OCTAVE = 0,
    MINOR_NINTH = 1,
    MAJOR_NINTH = 2,
    MINOR_TENTH = 3,
    MAJOR_TENTH = 4,
    PERFECT_ELEVENTH = 5,
    SHARP_ELEVENTH = 6,
    PERFECT_TWELFTH = 7,
    MINOR_THIRTEENTH = 8,
    MAJOR_THIRTEENTH = 9
};

struct FamousChord
{
    std::vector<int> intervals;
    std::string name;
    std::string notation;

    FamousChord(std::initializer_list<int> notes, const std::string &name, const std::string &notation)
        : intervals(notes), name(name), notation(notation)
    {}
};

struct Chord
{
    Notes root;

    Chord()
    {}

    Chord(std::vector<int> interv);

    Chord(std::initializer_list<NoteIntervals> ints)
        : intervals(ints)
    {}

    void AddNoteInterval(NoteIntervals interval);

    void Clear();

    std::vector<Notes> GetNotes();

    std::string GetName();

    std::string GetNotation();

    bool Matches(FamousChord chord);

private:
    std::vector<NoteIntervals> intervals;

};

static FamousChord famousChords[] = {
    
    // Minors
    FamousChord({ UNISON, MINOR_THIRD, MAJOR_THIRD, TRITONE }, "Diminished Hanging Third", "3?dim"),
    FamousChord({ UNISON, MINOR_THIRD, MAJOR_THIRD, PERFECT_FIFTH }, "Hanging Third", "3?"),
    FamousChord({ UNISON, MINOR_THIRD, TRITONE }, "Diminished", "dim"),
    FamousChord({ UNISON, MINOR_THIRD, TRITONE, PERFECT_FIFTH }, "Minor Hanging Fifth", "m5?"),
    FamousChord({ UNISON, MINOR_THIRD, TRITONE, MAJOR_SIXTH }, "Diminished Seventh", "dim7"),
    FamousChord({ UNISON, MINOR_THIRD, TRITONE, MINOR_SEVENTH }, "Minor Seventh Flat Five", "m7b5"),
    FamousChord({ UNISON, MINOR_THIRD, TRITONE, MINOR_NINTH }, "Diminished Add Flat Ninth", "dim+b9"),
    FamousChord({ UNISON, MINOR_THIRD, TRITONE, MAJOR_NINTH }, "Diminished Add Ninth", "dim+9"),
    FamousChord({ UNISON, MINOR_THIRD, TRITONE, PERFECT_ELEVENTH }, "Diminished Add Eleventh", "dim+11"),
    FamousChord({ UNISON, MINOR_THIRD, TRITONE, MINOR_THIRTEENTH }, "Diminished Add Flat Thirteen", "dim+b13"),
    FamousChord({ UNISON, MINOR_THIRD, PERFECT_FIFTH }, "Minor", "m"),
    FamousChord({ UNISON, MINOR_THIRD, PERFECT_FIFTH, MAJOR_SIXTH }, "Minor Sixth", "m6"),
    FamousChord({ UNISON, MINOR_THIRD, PERFECT_FIFTH, MAJOR_SIXTH, MAJOR_NINTH }, "Minor Sixth Add Ninth", "m6+9"),
    FamousChord({ UNISON, MINOR_THIRD, PERFECT_FIFTH, MINOR_SEVENTH }, "Minor Seventh", "m7"),
    FamousChord({ UNISON, MINOR_THIRD, PERFECT_FIFTH, MINOR_SEVENTH, MAJOR_NINTH }, "Minor Ninth", "m9"),
    FamousChord({ UNISON, MINOR_THIRD, PERFECT_FIFTH, MINOR_SEVENTH, MAJOR_NINTH, PERFECT_ELEVENTH }, "Minor Eleventh", "m11"),
    FamousChord({ UNISON, MINOR_THIRD, PERFECT_FIFTH, MAJOR_SEVENTH }, "Minor Major Seventh", "mM7"),
    FamousChord({ UNISON, MINOR_THIRD, PERFECT_FIFTH, MAJOR_SEVENTH, MAJOR_NINTH }, "Minor Major Ninth", "mM9"),
    FamousChord({ UNISON, MINOR_THIRD, PERFECT_FIFTH, MAJOR_NINTH }, "Minor Add Ninth", "m+9"),
    FamousChord({ UNISON, MINOR_THIRD, PERFECT_FIFTH, PERFECT_ELEVENTH }, "Minor Add Eleventh", "m+11"),
    FamousChord({ UNISON, MINOR_THIRD, PERFECT_FIFTH, MINOR_THIRTEENTH }, "Minor Add Flat Thirteenth", "m+b13"),

    // Majors
    FamousChord({ UNISON, MAJOR_THIRD, TRITONE }, "Major Flat Five", "b5"),
    FamousChord({ UNISON, MAJOR_THIRD, TRITONE, PERFECT_FIFTH }, "Major Hanging Fifth", "5?"),
    FamousChord({ UNISON, MAJOR_THIRD, TRITONE, MINOR_SEVENTH }, "Dominant Seventh Flat Five", "7b5"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH }, "Major", ""),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MAJOR_SIXTH }, "Major Sixth", "6"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MAJOR_SIXTH, MAJOR_NINTH }, "Major Sixth Add Ninth", "6+9"),
    // Dominant 7
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MINOR_SEVENTH }, "Dominant Seventh", "7"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MINOR_SEVENTH, MINOR_NINTH }, "Dominant Flat Ninth", "_b9"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MINOR_SEVENTH, MINOR_NINTH, MINOR_THIRTEENTH }, "Dominant Flat Ninth Add Flat Thirteenth", "b9+b13"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MINOR_SEVENTH, MINOR_NINTH, MAJOR_THIRTEENTH }, "Dominant Flat Ninth Add Thirteenth", "b9+13"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MINOR_SEVENTH, MAJOR_NINTH }, "Dominant Ninth", "7(9)"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MINOR_SEVENTH, MAJOR_NINTH, SHARP_ELEVENTH }, "Dominant Sharp Eleventh", "#11"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MINOR_SEVENTH, MAJOR_NINTH, SHARP_ELEVENTH, MAJOR_THIRTEENTH }, "Dominant Sharp Eleventh Add Thirteenth", "#11+13"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MINOR_SEVENTH, MAJOR_NINTH, MAJOR_THIRTEENTH }, "Dominant Ninth Add Thirteenth", "9+13"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MINOR_SEVENTH, MINOR_TENTH }, "Dominant Sharp Ninth", "#9"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MINOR_SEVENTH, MINOR_TENTH, MINOR_THIRTEENTH }, "Dominant Sharp Ninth Add Flat Thirteenth", "#9+b13"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MINOR_SEVENTH, MINOR_TENTH, MAJOR_THIRTEENTH }, "Dominant Sharp Ninth Add Thirteenth", "#9+13"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MINOR_SEVENTH, MINOR_THIRTEENTH }, "Dominant Add Flat Thirteenth", "7+b13"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MINOR_SEVENTH, MAJOR_THIRTEENTH }, "Dominant Add Thirteenth", "7+13"),
    // Major 7
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MAJOR_SEVENTH }, "Major Seventh", "M7"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MAJOR_SEVENTH, MAJOR_NINTH }, "Major Ninth", "M9"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MAJOR_SEVENTH, MAJOR_NINTH, SHARP_ELEVENTH }, "Major Sharp Eleventh", "M#11"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MAJOR_SEVENTH, MAJOR_NINTH, MAJOR_THIRTEENTH }, "Major Ninth Add Thirteenth", "M9+13"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MAJOR_SEVENTH, MAJOR_THIRTEENTH }, "Major Seventh Add Thirteenth", "M7+13"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MINOR_NINTH }, "Major Add Flat Ninth", "+b9"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MAJOR_NINTH }, "Major Add Ninth", "+9"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, PERFECT_ELEVENTH }, "Major Add Eleventh", "+11"),
    FamousChord({ UNISON, MAJOR_THIRD, PERFECT_FIFTH, MINOR_THIRTEENTH }, "Major Add Flat Thirteenth", "+b13"),

    // Augmented
    FamousChord({ UNISON, MAJOR_THIRD, MINOR_SIXTH }, "Augmented", "aug"),
    FamousChord({ UNISON, MAJOR_THIRD, MINOR_SIXTH, MINOR_SEVENTH }, "Augmented Seventh", "aug7"),
    FamousChord({ UNISON, MAJOR_THIRD, MINOR_SIXTH, MAJOR_SEVENTH }, "Major Seventh Sharp Fifth", "M7#5"),
    FamousChord({ UNISON, MAJOR_THIRD, MINOR_SIXTH, MINOR_NINTH }, "Augmented Add Flat Ninth", "aug+b9"),
    FamousChord({ UNISON, MAJOR_THIRD, MINOR_SIXTH, MAJOR_NINTH }, "Augmented Add Ninth", "aug+9"),
    FamousChord({ UNISON, MAJOR_THIRD, MINOR_SIXTH, MAJOR_SEVENTH, MAJOR_NINTH }, "Major Ninth Sharp Fifth", "M7#5"),
    FamousChord({ UNISON, MAJOR_THIRD, MINOR_SIXTH, PERFECT_ELEVENTH }, "Augmented Add Eleventh", "aug+11"),
    FamousChord({ UNISON, MAJOR_THIRD, MINOR_SIXTH, MINOR_THIRTEENTH }, "Augmented Add Flat Thirteenth", "aug+b13"),
    FamousChord({ UNISON, MAJOR_THIRD, MINOR_SIXTH, MAJOR_THIRTEENTH }, "Augmented Add Thirteenth", "aug+13"),

    // Suspended
    FamousChord({ UNISON, PERFECT_FOURTH, PERFECT_FIFTH }, "Suspended Fourth", "sus4"),
    FamousChord({ UNISON, PERFECT_FOURTH, PERFECT_FIFTH, MINOR_SEVENTH }, "Seventh Suspended Fourth", "7sus4"),
};

#endif // !GROUND_CHORD_H
