#pragma once

#include "Note.h"

struct NoteRange
{
    Note btm;
    Note top;

    NoteRange(Note lowest, Note highest)
    {
        btm = lowest;
        top = highest;
    }
};

// Wind instruments
static NoteRange PiccoloRange(Note(C, 5), Note(D, 8));
static NoteRange Flute(Note(C, 4), Note(D, 7));
static NoteRange Oboe(Note(C, 4), Note(G, 6));
static NoteRange Clarinet(Note(E, 3), Note(G, 6));
static NoteRange Trumpet(Note(E, 3), Note(B, 5));
static NoteRange FrenchHorn(Note(A, 2), Note(A, 5));
static NoteRange Trombone(Note(E, 2), Note(B, 4));
static NoteRange BassClarinet(Note(E, 2), Note(B, 4));
static NoteRange BassClarinet(Note(E, 2), Note(B, 4));
static NoteRange Bassoon(Note(B, 1), Note(B, 4));
static NoteRange Tuba(Note(F, 1), Note(F, 4));

// Percussion instruments
static NoteRange Timpani(Note(F, 2), Note(F, 3));

// String instruments
static NoteRange Violin(Note(G, 3), Note(G, 7));
static NoteRange Viola(Note(C, 3), Note(D, 6));
static NoteRange Cello(Note(C, 2), Note(F, 5));
static NoteRange DoubleBass(Note(E, 1), Note(B, 3));

// Human voices
static NoteRange Soprano(Note(B, 3), Note(D, 5));
static NoteRange Alto(Note(F, 3), Note(F, 5));
static NoteRange Tenor(Note(C, 3), Note(B, 4));
static NoteRange Baritone(Note(G, 2), Note(G, 4));
static NoteRange Bass(Note(E, 2), Note(F, 4));