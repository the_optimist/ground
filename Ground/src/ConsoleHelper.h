#ifndef GROUND_CONSOLE_H
#define GROUND_CONSOLE_H

#include <string>
#include <vector>
#include <deque>

class Console
{
public:
    Console() {}
    ~Console() {}

    static void Tell(const std::string& s, bool endl = true);

    static void TellF(const char *format, ...);

    static const int IntAsk(const std::string& q, bool endl = true);

    static const std::string StrAsk(const std::string &q, bool endl = true);

    static const bool BoolAsk(const std::string &q, bool endl = true);

private:
    void PushString(const std::string &s);

    std::deque<std::string> strings;
};


const bool StrToInt(const std::string &s, int *out);

std::vector<std::string> StrSplit(const std::string &s, char delimiter);



#endif //!GROUND_CONSOLE_H