#include "BasicStates.h"

#include "ConsoleHelper.h"

MenuState::MenuState()
{

}

MenuState::~MenuState()
{

}

void MenuState::Init()
{
    Console::Tell("Welcome to Ground!");
}

void MenuState::OnEnter()
{
    Console::Tell("---MAIN MENU---");
    playState = false;
    testState = false;
    quitState = false;
}

void MenuState::OnExit()
{
    Console::Tell("");
}

void MenuState::Update()
{
    Console::Tell("0) Play!");
    Console::Tell("1) Test");
    Console::Tell("2) Exit");
    int i = Console::IntAsk("Choice: ", false);
    switch (i)
    {
    case 0:
        playState = true;
        break;
    case 1:
        testState = true;
        break;
    case 2:
        quitState = true;
        break;
    default:
        break;
    }
}