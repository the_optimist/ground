#ifndef GROUND_PROBABILITY_H
#define GROUND_PROBABILITY_H

#include <vector>
#include <cmath>

class RandomSample
{
public:
    float mean;
    float variance;
    float standardDeviation;

    std::vector<float> samples;
    int N;

    RandomSample()
        : mean(0.0f), variance(0.0f), standardDeviation(0.0f),
        samples(), N(0)
    {}

    RandomSample(float *data, int count)
        : mean(0.0f), variance(0.0f), standardDeviation(0.0f),
        samples(), N(count)
    {
        for (int i = 0; i != count; ++i)
        {
            const float x = data[i];
            samples.push_back(x);
            xSum += x;
            xSquareSum += pow(x, 2);
        }

        Refresh();
    }

    void AddData(const float x)
    {
        samples.push_back(x);
        xSum += x;
        xSquareSum += pow(x, 2);
        ++N;

        Refresh();
    }

    void AddData(float *data, const int count)
    {
        for (int i = 0; i != count; ++i)
        {
            const float x = data[i];
            samples.push_back(x);
            xSum += x;
            xSquareSum += pow(x, 2);
        }

        N += count;

        Refresh();
    }

private:
    float Refresh()
    {
        mean = xSum / N;
        variance = (1.0f / (N - 1)) * (xSquareSum - pow(xSum, 2) / N);
        standardDeviation = sqrtf(variance);
    }

    float xSum;
    float xSquareSum;
};

class Histogram
{
public:
    Histogram()
        : binCount(256), bins(binCount), min(-1), max(1)
    {}

    Histogram(int _binCount, float minVal, float maxVal)
        : binCount(_binCount), bins(_binCount), min(minVal), max(maxVal)
    {}

    Histogram(int _binCount, float minVal, float maxVal, RandomSample *sample)
        : binCount(_binCount), bins(_binCount), min(minVal), max(maxVal)
    {
        AddSample(sample);
    }

    Histogram(int _binCount, float minVal, float maxVal, float *sample, int N)
        : binCount(_binCount), bins(_binCount), min(minVal), max(maxVal)
    {
        AddSample(sample, N);
    }

    void AddSample(RandomSample *sample)
    {
        const float indexer = binCount / (max - min);

        for (float x : sample->samples)
        {
            ++bins[(int)indexer * (x - min)];
        }
    }

    void AddSample(float *sample, int N)
    {
        const float indexer = binCount / (max - min);

        for (int i = 0; i != N; ++i)
        {
            ++bins[(int)indexer * (sample[i] - min)];
        }
    }

    int SuggestBinCount(int sampleCount)
    {
        return (int)sqrt(sampleCount);
    }

private:
    const int binCount;
    std::vector<int> bins;

    const float min;
    const float max;
};

#endif // !GROUND_PROBABILITY_H