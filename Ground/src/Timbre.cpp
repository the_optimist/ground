#include "Timbre.h"

#include <cmath>
#include "Constants.h"
#include "UtilityMath.h"
#include "Random.h"

// TODO(Martin) : We really need some wavetable synthesis here! :)

Timbre::Timbre()
{
}

Timbre::Timbre(Random *rng)
{
    for (int i = 0;
    i < WAVETABLE_SIZE;
        ++i)
    {
        wavetable[i] = rng->NextFloat();
        wavetable[i] *= std::sinf(TWOPIF * i / (float)(WAVETABLE_SIZE));
    }
}

Timbre::Timbre(std::vector<float> amps)
    //: amplitudes(amps)
{
    // Normalize the vector in the 1-norm to avoid peaking > 1
    float sum = 0;
    for (float a : amps)
    {
        sum += a;
    }

    for (int i = 0;
        i < WAVETABLE_SIZE;
        ++i)
    {
        float result = 0.0f;

        int k = 0;
        for (float &a : amps)
        {
            ++k;
            result += a * std::sinf(k * TWOPIF * i / (float)(WAVETABLE_SIZE)) / sum;
        }

        wavetable[i] = result;
    }
}

const float Timbre::GetImpulse(float phase) const
{
    float t = WAVETABLE_SIZE * (phase);
    int wavetableIndex = (int)t;

    t -= wavetableIndex;

    if (wavetableIndex != WAVETABLE_SIZE)
        return Lerp(wavetable[wavetableIndex], wavetable[wavetableIndex + 1], t);
    else // wrap around
        return Lerp(wavetable[wavetableIndex], wavetable[0], t);
}