#ifndef GROUND_STATE_MACHINE_H
#define GROUND_STATE_MACHINE_H

#include <vector>

class State;

struct Transition
{
    bool *condition;
    State *state;
};

class State
{
public:
    friend class StateMachine;

    State()
        : initialized(false)
    {}

    virtual ~State() {}

    void AddTransition(Transition t)
    {
        transitions.push_back(t);
    }

    virtual void Init() {}
    virtual void OnEnter() {}
    virtual void OnExit() {}
    virtual void Update() = 0;

private:
    bool initialized;
    std::vector<Transition> transitions;
};

class StateMachine
{
public:
    StateMachine();
    ~StateMachine();

    void SetState(State *state);

    void Update();

private:
    State *nowState;
};

#endif // !GROUND_STATE_MACHINE_H