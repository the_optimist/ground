#ifndef GROUND_UTILITY_MATH_H
#define GROUND_UTILITY_MATH_H

template<typename T>
const T Lerp(const T a, const T b, const float t)
{
    // floating point arithmetic hax for better precision!
    return (1 - t)*a + t*b; // a+t(b-a) is imprecise
}

template<typename T>
const T Clamp(const T x, const T m, const T M)
{
    if (x < m)
        return m;
    if (x > M)
        return M;
    return x;
}

#endif // !GROUND_UTILITY_MATH_H
