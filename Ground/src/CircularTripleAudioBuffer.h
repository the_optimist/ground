#ifndef GROUND_CIRCULAR_TRIPLE_BUFFER_H
#define GROUND_CIRCULAR_TRIPLE_BUFFER_H

#include <memory>

#include "Constants.h"

struct CircularTripleAudioBuffer
{
    float buffers[3][SAMPLE_RATE];

    CircularTripleAudioBuffer()
    {
        memset(buffers, 0, 3 * SAMPLE_RATE);
    }

    float *GetNextBufferStart()
    {
        return &(buffers[(currentBufferIndex + 1) % 3][0]);
    }

    float *GetCurrentBufferEnd()
    {
        return &(buffers[currentBufferIndex][SAMPLE_RATE]);
    }

    void SwapBuffer()
    {
        currentBufferIndex = (currentBufferIndex + 1) % 3;
    }

    void CopyFromBuffer(void *dst, const size_t size)
    {
        memcpy(dst, &buffers[currentBufferIndex][0], size);
    }

    void Clear()
    {
        memset(buffers, 0, 3 * SAMPLE_RATE);
    }

private:
    int currentBufferIndex = 0;
};

#endif // !GROUND_CIRCULAR_TRIPLE_BUFFER_H