#ifndef GROUND_NOTE_H
#define GROUND_NOTE_H

#include<cmath>

#include<string>

// All 12 standard notes
enum Notes
{
    C,
    D_FLAT,
    D,
    E_FLAT,
    E,
    F,
    F_FLAT,
    G,
    A_FLAT,
    A,
    B_FLAT,
    B
};

// The frequencies for all MIDI notes
static float freqs[] = {
    8.17579891564368f, // C-1 (First MIDI note)
    8.66195721802722f, // Db-1
    9.17702399741896f, // D-1
    9.722718241315f, // Eb-1
    10.3008611535272f, // E-1
    10.9133822322813f, // F-1
    11.5623257097385f, // Gb-1
    12.2498573744296f, // G-1
    12.9782717993732f, // Ab-1
    13.75f, // A-1
    14.5676175474403f, // Bb-1
    15.4338531642538f, // B-1
    16.3515978312874f, // C0
    17.3239144360545f, // Db0
    18.6540479948379f, // D0
    19.44543648263f, // Eb0
    20.6017223070543f, // E0
    21.8567644645627f, // F0
    23.1246514194771f, // Gb0
    24.4997147488593f, // G0
    25.9565435987465f, // Ab0
    27.5f, // A0 (Piano low A)
    29.1352350948806f, // Bb0
    30.8677063285077f, // B0 (5-string bass low open B)
    32.7031956625748f, // C1
    34.6478288721089f, // Db1
    36.7080959896759f, // D1
    38.89087296526f, // Eb1
    41.2034446141087f, // E1 (4-string bass low open E)
    43.6535289291254f, // F1
    46.2493028389542f, // Gb1
    48.9994294977186f, // G1
    51.913087197493f, // Ab1
    55.0f, // A1
    58.2704701897611f, // B1
    61.7354126570154f, // B1
    65.4063913251495f, // C2
    69.2956577442179f, // Db2
    73.4161919793518f, // D2
    77.7817459305201f, // Eb2
    82.4068892282174f, // E2 (Guitar low open E)
    87.3070578582508f, // F2
    92.4986056779085f, // Gb2
    97.9988589954372f, // G2 (4 or 5 string bass, high open G)
    103.826174394986f, // Ab2
    110.0f, // A2 (Guitar 5th string open)
    116.540940379522f, // Bb2
    123.470825314031f, // B2
    130.812782650299f, // C3 (6-string bass, high open C)
    138.591315488436f, // Db3
    146.832383958704f, // D3 (Guitar 4th string open)
    155.56349186104f, // Eb3
    164.813778456435f, // E3
    174.614115716502f, // F3
    184.997211355817f, // Gb3
    195.997717990874f, // G3 (Guitar 3rd string open) (Violin low open G)
    207.652348789972f, // Ab3
    220.0f, // A3
    233.081880759045f, // Bb3
    246.941650628062f, // B3 (Guitar 3rd string open)
    261.625565300599f, // C4 (Piano middle C)
    277.182630976872f, // Db4
    293.664767917407f, // D4 (Violin 3rd string open)
    311.126983722081f, // Eb4
    329.62755691287f, // E4 (Guitar 1st string open)
    349.228231433004f, // F4
    369.994422711634f, // Gb4
    391.995435981749f, // G4
    415.304697579945f, // Ab4
    440.0f, // A4 (Tuning fork A) (Violin 2nd string open)
    466.16376151809f, // Bb4
    493.883301256124f, // B4
    523.251130601197f, // C5
    554.365261953744f, // Db5
    587.329535834815f, // D5
    622.253967444162f, // Eb5
    659.25511382574f, // E5 (Guitar 1st string 12 fret) (Violin 1st string open)
    698.456462866008f, // F5
    739.988845423269f, // Gb5
    783.990871963499f, // G5
    830.609395159891f, // Ab5
    880.0f, // A5
    932.32752303618f, // Bb5
    987.766602512249f, // B5
    1046.50226120239f, // C6
    1108.73052390749f, // Db6
    1174.65907166963f, // D6
    1244.50793488832f, // Eb6
    1318.51022765148f, // E6 (Guitar 1st string 24 fret)
    1396.91292573202f, // F6
    1479.97769084654f, // Gb6
    1567.981743927f, // G6
    1661.21879031978f, // Ab6
    1760.0f, // A6
    1864.65504607236f, // Bb6
    1975.5332050245f, // B6
    2093.00452240479f, // C7
    2217.46104781498f, // Db7
    2349.31814333926f, // D7
    2489.01586977665f, // Eb7
    2637.02045530296f, // E7
    2793.82585146403f, // F7
    2959.95538169308f, // Gb7
    3135.963487854f, // G7
    3322.43758063956f, // Ab7
    3520.0f, // A7
    3729.31009214472f, // Bb7
    3951.066410049f, // B7
    4186.00904480958f, // C8 (Piano upper C)
    4434.92209562996f, // Db8
    4698.63628667853f, // D8
    4978.0317395533f, // Eb8
    5274.04091060593f, // E8
    5587.65170292807f, // F8
    5919.91076338616f, // Gb8
    6271.926975708f, // G8
    6644.87516127913f, // Ab8
    7040.00000000001f, // A8
    7458.62018428945f, // Bb8
    7902.132820098f, // B8
    8372.01808961917f, // C9
    8869.84419125992f, // Db9
    9397.27257335706f, // D9
    9956.06347910661f, // Eb9
    10548.0818212119f, // E9
    11175.3034058561f, // F9
    11839.8215267723f, // Gb9
    12543.853951416f, // G9 (Last MIDI note)
};

// basically a vector (notename,octave) which calculates the note frequency.
struct Note
{
    Notes index;
    int octave;
    float frequency;

    Note()
    {}

    Note(Notes ix, int oct)
        : index(ix), octave(oct)
    {
        Refresh();
    }

    static std::string GetNoteName(Notes note, Notes root)
    {
        static const std::string flatNames[12] =
        {
            "C",
            "Db",
            "D",
            "Eb",
            "E",
            "F",
            "Gb",
            "G",
            "Ab",
            "A",
            "Bb",
            "B",
        };

        static const std::string sharpNames[12] =
        {
            "C",
            "C#",
            "D",
            "D#",
            "E",
            "F",
            "F#",
            "G",
            "G#",
            "A",
            "A#",
            "B",
        };

        if (root == G ||
            root == D ||
            root == A ||
            root == E ||
            root == B)
            return sharpNames[note];

        return flatNames[note];
    }

    // Calculates the frequency from (note, octave)
    void Refresh()
    {
        int i = (int)index + (octave + 1) * 12;

        const static int max = (sizeof(freqs) / sizeof(float)) - 1;

        if (i < 0)
        {
            frequency = freqs[0];
        }
        else if (i > max)
        {
            frequency = freqs[max];
        }
        else
        {
            frequency = freqs[i];
        }
    }

    // Operator overloads
    bool operator ==(const Note& rhs)
    {
        return (index == rhs.index && octave == rhs.octave);
    }

    bool operator !=(const Note& rhs)
    {
        return (index != rhs.index || octave != rhs.octave);
    }

    void operator +=(const int inc)
    {
        int ix = (int)index + inc;
        if (inc >= 0)
        {
            while (ix >= 12)
            {
                ix -= 12;
                ++octave;
            }
            index = (Notes)ix;
        }
        else
        {
            while (ix < 0)
            {
                ix += 12;
                --octave;
            }
            index = (Notes)ix;
        }

        Refresh();
    }

    void operator -=(const int inc)
    {
        int ix = (int)index - inc;
        if (inc < 0)
        {
            while (ix >= 12)
            {
                ix -= 12;
                ++octave;
            }
            index = (Notes)ix;
        }
        else
        {
            while (ix < 0)
            {
                ix += 12;
                --octave;
            }
            index = (Notes)ix;
        }

        Refresh();
    }
};

struct PlayedNoteInfo
{
    Note note;
    float velocity;

    PlayedNoteInfo(Note _note, float _velocity)
    {
        note = _note;
        velocity = _velocity;
    }
};

#endif // !GROUND_NOTE_H
