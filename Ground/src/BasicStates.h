#ifndef GROUND_BASIC_STATES_H
#define GROUND_BASIC_STATES_H

#include "StateMachine.h"

class MenuState : public State
{
public:
    MenuState();
    virtual ~MenuState();

    bool playState;
    bool testState;
    bool quitState;

    virtual void Init();
    virtual void OnEnter();
    virtual void OnExit();
    virtual void Update();

private:

};

class TestState : public State
{
public:
    TestState();
    virtual ~TestState();

    bool menuState;

    virtual void Init();
    virtual void OnEnter();
    virtual void OnExit();
    virtual void Update();

private:

};

class PlayState : public State
{
public:
    PlayState();
    virtual ~PlayState();

    bool menuState;

    virtual void Init();
    virtual void OnEnter();
    virtual void OnExit();
    virtual void Update();

private:

};

#endif // !GROUND_BASIC_STATES_H
