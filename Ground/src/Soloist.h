#ifndef GROUND_SOLOIST_H
#define GROUND_SOLOIST_H

#include "Note.h"
#include "Random.h"
#include "Scale.h"

#include "ConsoleHelper.h"

// fw-decls
class Synth;
class Arranger;
class Composer;

enum ImprovisationType
{
    IT_CHORDS,
    IT_SOLO
};

class Soloist
{
public:
    Soloist(int seed, int bottomOctave, int topmostOctave, ImprovisationType impType);

    Soloist()
    {}

    virtual ~Soloist()
    {}
    
    void Improvise(Synth *synth, Arranger *arranger, Composer *composer);

private:
    std::vector<PlayedNoteInfo> playedNotes;

    int feel;
    int feelCounter;
    float noteTimer;
    float noteTime;

    bool isQuiet;

    int btmOctave;
    int topOctave;

    Random rng;

    ImprovisationType type;
};

#endif // !GROUND_SOLOIST_H
