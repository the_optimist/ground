#include "ConsoleHelper.h"
#include "OperatingSystem.h"

#include <string>
#include <iostream>
#include <vector>

#include <stdarg.h>

void Console::Tell(const std::string& s, bool endl)
{
    if (endl)
        std::cout << s << std::endl;
    else
        std::cout << s;
}

void Console::TellF(const char *format, ...)
{
    char buffer[256];
    va_list args;
    va_start(args, format);
    vsprintf_s(buffer, format, args);
    va_end(args);
    
    Tell(buffer);
}

const int Console::IntAsk(const std::string& q, bool endl)
{
    Tell(q, endl);

    int a = 0;
    std::cin >> a;

    return a;
}

const std::string Console::StrAsk(const std::string &q, bool endl)
{
    Tell(q, endl);

    std::cin.clear();
    // Make sure we get new input by throwing away all the hogwash up until newline.
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    
    std::string a;
    std::getline(std::cin, a);

    return a;
}

const bool Console::BoolAsk(const std::string &q, bool endl)
{
    std::string a = StrAsk(q + " (Y/N)", endl);

    return (a[0] == 'y' || a[0] == 'Y');
}

void Console::PushString(const std::string &s)
{
    strings.push_back(s);
    if (USING_WINDOWS)
    {

    }
    //if (strings.size() > )
    strings.pop_front();
}

const bool StrToInt(const std::string &s, int *out)
{
	int value = 0;

	const unsigned char ASCII_ZERO_VALUE = 48;

	// Read string backwards
	for (auto &c = s.crbegin(); c != s.crend(); ++c)
	{
		if (ASCII_ZERO_VALUE <= *c && *c < ASCII_ZERO_VALUE + 10)
		{
			// multiply by ten and add the integer value
			value = value * 10 + (*c - ASCII_ZERO_VALUE);
		}
		else
		{
			return false; // if char is not a numeric, fail
		}
	}

	*out = value;
	return true;
}

std::vector<std::string> StrSplit(const std::string &split, char delimiter)
{
	std::vector<std::string> strings;
	std::string current = "";

	for (size_t i = 0; i != split.size(); ++i)
	{
		char c = split[i];
		if (c == delimiter)
		{
			if (!current.empty())
			{
				strings.push_back(current);
				current = "";
			}
		}
		else
		{
			current += c;
		}
	}

    if (!current.empty())
        strings.push_back(current);

	return strings;
}