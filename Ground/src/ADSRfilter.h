#ifndef GROUND_ADSR_FILTER_H
#define GROUND_ADSR_FILTER_H

struct ADSRfilter
{
    float attackTime;
    float decayTime;
    float sustainLevel;
    float releaseTime;

    ADSRfilter()
        : attackTime(0.01f), decayTime(0.01f), sustainLevel(0.7f), releaseTime(0.2f)
    {}

    ADSRfilter(const float a, const float d, const float s, const float r)
        : attackTime(a), decayTime(d), sustainLevel(s), releaseTime(r)
    {}
};

#endif // !GROUND_ADSR_FILTER_H