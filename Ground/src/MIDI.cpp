#include "MIDI.h"

MIDIHeader ReadMIDIHeader(std::istream &r)
{
    MIDIHeader h;

    r.read((char*)&h, sizeof(h));

    if (h.M != 'M' || h.T != 'T' || h.h != 'h' || h.d != 'd')
        throw std::runtime_error("Invalid MIDI header");

    printf("%d", h.headerLength); // Not 6, bit magic is needed!

    return h;
}

void TestMIDIRead()
{
    std::ifstream r = std::ifstream("restart.mid");
    MIDIHeader h = ReadMIDIHeader(r);

    //for (int i = 0; i != h.n; ++i)
    //{
    //    char c;
    //    r.read(&c, 1);
    //}

    r.close();
}