#ifndef GROUND_OPERATING_SYSTEM_H
#define GROUND_OPERATING_SYSTEM_H

const static bool USING_WINDOWS =
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
true;
#else
false;
#endif

#endif // !GROUND_OPERATING_SYSTEM_H
