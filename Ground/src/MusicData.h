#ifndef GROUND_MUSIC_DATA_H
#define GROUND_MUSIC_DATA_H

struct MusicDataTemp
{
    // basic 
    float volume;
    float pan;

    // playing
    std::vector<float> noteVelocities;
    std::vector<Note> notes;
    Note prevNote;
    //float interpolation; // glissando
    Scale scale;

    // timbre
    Synth *synth;

    // used for events, such as note switching
    float timer;

    // rhythm
    int feel;
    int feelcounter;
    float bpm;
    bool play;

};

#endif // !GROUND_MUSIC_DATA_H